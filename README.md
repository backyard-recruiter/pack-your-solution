# Instruction: 

1. Clone the project 
2. Finish the problem,
    - Understand the problem
    - Fill find_missing method in main.py
    - Fill Dockerfile
    - Check your solution by executing `./run.sh`, it should look like this:

    ```
    $ ./run.sh
    Sending build context to Docker daemon  1.16MB
    Step 1/4 : FROM python:3.8
    ---> 659f826fabf4
    ...
    ... These should be your Dockerfile statements
    ...
    Successfully built 986721581934
    Successfully tagged backyard/findmissing:latest
    ..
    ----------------------------------------------------------------------
    Ran 2 tests in 0.001s

    OK
    ```
3. Zip your solution and send to our HR email 

---

# Problem

## Find the missing term in an Arithmetic Progression

An Arithmetic Progression is defined as one in which there is a constant difference between the consecutive terms of a given series of numbers. You are provided with consecutive elements of an Arithmetic Progression. There is however one hitch: exactly one term from the original series is missing from the set of numbers which have been given to you. The rest of the given series is the same as the original AP. Find the missing term.

You have to write a function that receives a list, list size will always be at least 3 numbers. The missing term will never be the first or last one.

### Example
find_missing([1, 3, 5, 9, 11]) == 7