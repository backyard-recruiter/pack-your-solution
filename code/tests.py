import unittest

from main import find_missing


class TestFindMissing(unittest.TestCase):
    def test_one(self):
        self.assertEqual(find_missing([1, 2, 3, 4, 6, 7, 8, 9]), 5)
    
    def test_two(self):
        self.assertEqual(find_missing([-1, -4, -7, -10, -13, -16, -19, -22, -28]), -25)